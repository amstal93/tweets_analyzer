# tweets_analyzer

A Docker image for [x0rz/tweets_analyzer](https://github.com/x0rz/tweets_analyzer).

This is mostly a packaging convenience,
since this tool requires `numpy==1.15.1` which fails to build on python 3.9.

## Usage

Create a `secrets.py` and follow its instructions:

```python
# Go to https://apps.twitter.com/ and create an app.
# The consumer key and secret will be generated for you after
consumer_key="xxxxxxxxxxxxxx"
consumer_secret="xxxxxxxxxxxxx"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Create New App" section
access_token="xxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
access_token_secret="xxxxxxxxxxxxxxxxxxxxxxx"
```

Then, mount it at `/secrets.py`:

```shell
docker run --rm -it -v secrets.py:/secrets.py \
    letompouce/tweets_analyzer -n <screen_name> [options]
```

### curl|sh in a container world

Convenient `.bashrc` alias:

```shell
[ -r "${HOME}/.config/tweets_analyzer.secrets.py" ] && \
    tweets_analyzer() {
        docker run \
            --rm -it \
            --name tweets_analyzer \
            -u $(id -u):$(id -g) \
            -v "${HOME}/.config/tweets_analyzer.secrets.py:/secrets.py" \
            letompouce/tweets_analyzer \
            "$@"
    }
```

## Links

* Source: <https://gitlab.com/l3tompouce/docker/tweets_analyzer>
* Image: <https://hub.docker.com/r/letompouce/tweets_analyzer>
